﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LED_Engine_PBR
{
    public enum DrawMode
    {
        Default,
        Depth,
        BaseColor,
        Normals,
        Roughness,
        Metalness,
        AmbientOclusion,
        Emissive,
        Height,
        Light,
        BRDF_LUT,
        IBL_Diffuse,
        IBL_Specular,
    };

    public enum MeshType
    {
        Mesh,
        Plain,
        Box,
    };
}
