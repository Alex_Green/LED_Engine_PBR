		Textures/CubemapTextures
----------------------------------------

Name:
	string - Any unique name.

[TextureCubemap] Dir:
	string - Folder, not necessary, this folder contain 'File[]'.
[TextureCubemap] File[]
	string - {X+, X-, Y+, Y-, Z+, Z-} - 6 textures for Cubemap (see examples), or 1 file.

[Texture2D] File
	string - FileName to texture, see examples.

bool sRGB;

bool Mipmaps;

bool AnisotropicFiltering;

MagFilter:
	Nearest
	Linear
	LinearDetailSgis
	LinearDetailAlphaSgis
	LinearDetailColorSgis
	LinearSharpenSgis
	LinearSharpenAlphaSgis
	LinearSharpenColorSgis
	Filter4Sgis
	PixelTexGenQCeilingSgix
	PixelTexGenQRoundSgix
	PixelTexGenQFloorSgix

MinFilter:
	Nearest
	Linear
	NearestMipmapNearest
	LinearMipmapNearest
	NearestMipmapLinear
	LinearMipmapLinear
	Filter4Sgis
	LinearClipmapLinearSgix
	PixelTexGenQCeilingSgix
	PixelTexGenQRoundSgix
	PixelTexGenQFloorSgix
	NearestClipmapNearestSgix
	NearestClipmapLinearSgix
	LinearClipmapNearestSgix
	
TextureWrapS,
TextureWrapR,
TextureWrapT:
	Clamp,
	ClampToBorder,
	ClampToBorderSgis,
	ClampToEdge,
	ClampToEdgeSgis,
	MirroredRepeat,
	Repeat
	
--------------------------------
			Examples			
--------------------------------
<Texture>
	<Name>Wall</Name>
	<File>Wall.png</File>
</Texture>
--------------------------------
<CubemapTexture>
	<Name>SkyCubemap_Default</Name>
	<Dir>Test</Dir>
	<File>posx.jpg</File>
	<File>negx.jpg</File>
	<File>posy.jpg</File>
	<File>negy.jpg</File>
	<File>posz.jpg</File>
	<File>negz.jpg</File>
</CubemapTexture>
--------------------------------