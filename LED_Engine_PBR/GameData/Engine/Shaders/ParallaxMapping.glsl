uniform float Parallax_Scale = 0.08;		// Default: 0.04 - 0.12
uniform float Parallax_MinSteps = 32.0;		// Default:  8 .. 32
uniform float Parallax_MaxSteps = 64.0;		// Default: 32 .. 128
uniform bool Parallax_DiscardFragments = false;	// Discard fragment render by UV coords, default false.

void ParallaxMapping_DiscardByUVs(vec2 Original_UV, vec2 UV_Offset)
{
	vec2 Fract_UV = fract(Original_UV);
	vec2 UV = mix(vec2(1.0) - Fract_UV, Fract_UV, step(vec2(0.0), sign(Original_UV))) + UV_Offset;
	
	if (UV.x < 0.0 || UV.x > 1.0 || UV.y < 0.0 || UV.y > 1.0)
		discard;
}

// Camera vector World Space (vector from surface to camera)
// Camera vector Tangent Space
// Vertex normal World Space
// Texture Coordinates and their derivatives
vec2 ParallaxMapping(sampler2D Texture, vec3 CameraTS, vec3 CameraWS, vec3 NormalWS, vec2 UV, vec2 UV_DDX, vec2 UV_DDY)
{
	float StepsFactor = clamp(abs(dot(CameraWS, NormalWS)), 0.0, 1.0);
	float StepsMaxCount = mix(Parallax_MaxSteps, Parallax_MinSteps, StepsFactor);
	float StepSize = 1.0 / StepsMaxCount;
	vec2 Distance = (CameraTS.xy / CameraTS.z) * Parallax_Scale * StepSize;
	
	float Ray_Height = 1.0;
	float Ray_Height_Old = 1;
	float TextureHeight_Old = 1.0;
	vec2  UV_Offset = vec2(0.0);
	
	for (int i = 0; i <= floor(StepsMaxCount) + 1; i++)
	{
		float TextureHeight = textureGrad(Texture, UV + UV_Offset, UV_DDX, UV_DDY).r;
		
		if (Ray_Height < TextureHeight)
		{
			float DeltaHeight = TextureHeight - Ray_Height;
			float DeltaHeight_Old = Ray_Height_Old - TextureHeight_Old;
			float Intersection = DeltaHeight / (DeltaHeight + DeltaHeight_Old);
			UV_Offset += Distance * Intersection;
			break;
		}
		
		Ray_Height_Old = Ray_Height;
		Ray_Height -= StepSize;
		TextureHeight_Old = TextureHeight;
		UV_Offset -= Distance;
	}
	
	if (Parallax_DiscardFragments)
		ParallaxMapping_DiscardByUVs(UV, UV_Offset);
	
	return UV + UV_Offset;
}