#version 330
uniform sampler2D TextureUnit1;

in vec2 f_UV;

layout(location = 0) out vec4 FragColor;

#include("SRGB_To_Linear.glsl")

void main()
{
	float Roughness = texture(TextureUnit1, f_UV).a;
	Roughness = sRGB_To_Linear(Roughness);
	FragColor = vec4(Roughness, Roughness, Roughness, 1.0);
}