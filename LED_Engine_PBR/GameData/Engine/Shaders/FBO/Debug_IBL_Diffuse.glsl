#version 330
uniform sampler2D TextureUnit2; // Normal.xyz, Metalness
uniform samplerCube TextureUnit6; // IBL Diffuse  Environment

in vec2 f_UV;

layout(location = 0) out vec4 FragColor;

void main()
{
	vec3 Normal = texture(TextureUnit2, f_UV).rgb;
	vec3 IBL_Diffuse = texture(TextureUnit6, Normal).rgb;
	FragColor = vec4(IBL_Diffuse, 1.0);
}