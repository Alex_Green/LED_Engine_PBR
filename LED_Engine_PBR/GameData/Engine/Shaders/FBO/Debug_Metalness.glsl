#version 330
uniform sampler2D TextureUnit2;

in vec2 f_UV;

layout(location = 0) out vec4 FragColor;

#include("SRGB_To_Linear.glsl")

void main()
{
	float Metalness = texture(TextureUnit2, f_UV).a;
	Metalness = sRGB_To_Linear(Metalness);
	FragColor = vec4(Metalness, Metalness, Metalness, 1.0);
}