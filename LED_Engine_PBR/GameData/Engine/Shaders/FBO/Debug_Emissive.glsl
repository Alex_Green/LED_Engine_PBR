#version 330
uniform sampler2D TextureUnit4;

in vec2 f_UV;

layout(location = 0) out vec4 FragColor;

#include("SRGB_To_Linear.glsl")

void main()
{
	vec3 Emissive = texture(TextureUnit4, f_UV).rgb;
	Emissive = sRGB_To_Linear(Emissive);
	FragColor = vec4(Emissive, 1.0);
}