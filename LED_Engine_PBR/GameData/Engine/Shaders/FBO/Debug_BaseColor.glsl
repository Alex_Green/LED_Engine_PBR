#version 330
uniform sampler2D TextureUnit1;

in vec2 f_UV;

layout(location = 0) out vec4 FragColor;

#include("SRGB_To_Linear.glsl")

void main()
{
	vec3 BaseColor = texture(TextureUnit1, f_UV).rgb;
	BaseColor = sRGB_To_Linear(BaseColor);
	FragColor = vec4(BaseColor, 1.0);
}