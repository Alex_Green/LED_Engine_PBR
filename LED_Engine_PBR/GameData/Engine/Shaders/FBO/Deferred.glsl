#version 330
uniform sampler2D TextureUnit0; // Depth
uniform sampler2D TextureUnit1; // BaseColor.rgb, Roughness
uniform sampler2D TextureUnit2; // Normal.xyz, Metalness
uniform sampler2D TextureUnit3; // PositionW.xyz, AmbientOcclusion
uniform sampler2D TextureUnit4; // Emissive.rgb,  Height
uniform sampler2D TextureUnit5; // BRDF LUT
uniform samplerCube TextureUnit6; // IBL Diffuse  Environment
uniform samplerCube TextureUnit7; // IBL Specular Environment

in vec2 f_UV;

#define BRDF_LUT TextureUnit5
#define IBL_Diff TextureUnit6
#define IBL_Spec TextureUnit7
#include("Light.glsl")

///////////////////////////////////////////////////////////////////////////

layout(location = 0) out vec4 FragColor;

void main()
{
	vec3 Normal = texture(TextureUnit2, f_UV).rgb;
	vec3 Position = texture(TextureUnit3, f_UV).rgb;
	vec3 ViewDir = normalize(CameraPos - Position);	// Get View vector (from surface to camera)
	
	vec3 BaseColor = texture(TextureUnit1, f_UV).rgb;
	float Roughness = texture(TextureUnit1, f_UV).a;
	float Metalness = texture(TextureUnit2, f_UV).a;
	float AmbientOcclusion = texture(TextureUnit3, f_UV).a;
	vec3 Emissive = texture(TextureUnit4, f_UV).rgb;
	
	FragColor.rgb = CalcLight(Position, Normal, ViewDir, BaseColor, Roughness, Metalness, AmbientOcclusion, Emissive);
	FragColor.a = sqrt(dot(FragColor.rgb, vec3(0.299, 0.587, 0.114))); //Luma for FXAA
}