const float PACKNORMAL_SCALE = 1.7777;
const float PACKNORMAL_HALF_INV_SCALE = 1.0 / PACKNORMAL_SCALE;

vec2 PackNormal(vec3 Normal)
{
	vec3 N = normalize(Normal);
	return N.xy / (N.z + 1.0) * PACKNORMAL_HALF_INV_SCALE;
}

vec3 UnpackNormal(vec2 PackedNormal)
{
	vec2 NormalScaled = PackedNormal * PACKNORMAL_SCALE;
	float K = 2.0 / (dot(NormalScaled, NormalScaled) + 1.0);
	return normalize(vec3(K * NormalScaled, K - 1.0));
}