#version 330
in vec3 f_Position;
in vec2 f_UV;

layout(location = 0) out vec2 FragColor;

#include("..\FBO\SRGB_To_Linear.glsl")
#include("Functions.glsl");

// Geometry term
// http://graphicrants.blogspot.com.au/2013/08/specular-brdf-reference.html
// http://www.gamedev.net/topic/658769-ue4-ibl-glsl/
float GeometryGGX(float NdotV, float AlphaRoughness)
{
	// Schlick-Beckmann approximation and matched it to the GGX Smith formulation by remapping k:
	// Smith like when k = α/2 (UE4 like)
    float k = AlphaRoughness * 0.5;
    return NdotV / (NdotV * (1.0 - k) + k);

	/*
	// G GGX
	float r2 = AlphaRoughness * AlphaRoughness;
	return NdotV * 2 / (NdotV + sqrt((NdotV * NdotV) * (1.0 - r2) + r2));
	*/
}

void main() 
{
	float NdotV = f_UV.x;
	float Roughness = f_UV.y;
	float AlphaRoughness = Roughness * Roughness; // Disney's Roughness [see Burley'12 siggraph]
	float AlphaRoughnessSquared = AlphaRoughness * AlphaRoughness;
	
	vec3 N = vec3(0.0, 0.0, 1.0);
    vec3 V = vec3(sqrt(1.0 - NdotV * NdotV), 0.0, NdotV);
	mat3 TBN = ImportanceSampleTBN(N);
	
	float Vis = GeometryGGX(NdotV, AlphaRoughness); // LUT Visibility term
	
    float A = 0.0;
    float B = 0.0;
    
    const uint SAMPLE_COUNT = 1024u;
    for(uint i = 0u; i < SAMPLE_COUNT; i++)
    {
        // generates a sample vector that's biased towards the
        // preferred alignment direction (importance sampling).
        vec2 Xi = Hammersley(i, SAMPLE_COUNT);
        vec3 H = TBN * ImportanceSampleGGX(Xi, AlphaRoughnessSquared);
        vec3 L = -normalize(reflect(V, H));

        float NdotL = clamp(L.z, 0.0, 1.0);
        float NdotH = clamp(H.z, 0.0, 1.0);
        float VdotH = clamp(dot(V, H), 0.0, 1.0);

        if(NdotL > 0.0)
        {
            float G = Vis * GeometryGGX(NdotL, AlphaRoughness);	// LUT Geometry term
            float F = pow(1.0 - VdotH, 5.0);					// LUT Fresnel term
			float G_Vis = G * VdotH / (NdotH * NdotV);			// G * Vis => Smith G (Geometric Shadowing)
			
            A += (1.0 - F) * G_Vis;
            B += F * G_Vis;
        }
    }
    FragColor = vec2(A, B) / SAMPLE_COUNT;
}