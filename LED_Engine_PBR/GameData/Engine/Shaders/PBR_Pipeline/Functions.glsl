const float PI = 3.141592653589793;
const float TWO_PI = PI * 2.0;
const float FOUR_PI = PI * 4.0;
const float HALF_PI = PI * 0.5;
const float INV_PI = 1.0 / PI;

// Precomputed Hammersley arrays
const vec2 Hammersley16[16] = vec2[16](
	vec2(0.0, 0.0),
	vec2(0.0625, 0.5),
	vec2(0.125, 0.25),
	vec2(0.1875, 0.75),
	vec2(0.25, 0.125),
	vec2(0.3125, 0.625),
	vec2(0.375, 0.375),
	vec2(0.4375, 0.875),
	vec2(0.5, 0.0625),
	vec2(0.5625, 0.5625),
	vec2(0.625, 0.3125),
	vec2(0.6875, 0.8125),
	vec2(0.75, 0.1875),
	vec2(0.8125, 0.6875),
	vec2(0.875, 0.4375),
	vec2(0.9375, 0.9375)
);

const vec2 Hammersley32[32] = vec2[32](
	vec2(0.0, 0.0),
	vec2(0.03125, 0.5),
	vec2(0.0625, 0.25),
	vec2(0.09375, 0.75),
	vec2(0.125, 0.125),
	vec2(0.15625, 0.625),
	vec2(0.1875, 0.375),
	vec2(0.21875, 0.875),
	vec2(0.25, 0.0625),
	vec2(0.28125, 0.5625),
	vec2(0.3125, 0.3125),
	vec2(0.34375, 0.8125),
	vec2(0.375, 0.1875),
	vec2(0.40625, 0.6875),
	vec2(0.4375, 0.4375),
	vec2(0.46875, 0.9375),
	vec2(0.5, 0.03125),
	vec2(0.53125, 0.53125),
	vec2(0.5625, 0.28125),
	vec2(0.59375, 0.78125),
	vec2(0.625, 0.15625),
	vec2(0.65625, 0.65625),
	vec2(0.6875, 0.40625),
	vec2(0.71875, 0.90625),
	vec2(0.75, 0.09375),
	vec2(0.78125, 0.59375),
	vec2(0.8125, 0.34375),
	vec2(0.84375, 0.84375),
	vec2(0.875, 0.21875),
	vec2(0.90625, 0.71875),
	vec2(0.9375, 0.46875),
	vec2(0.96875, 0.96875)
);

// http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
// efficient VanDerCorpus calculation.
float RadicalInverse_VdC(uint bits) 
{
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

vec2 Hammersley(uint i, uint N)
{
	return vec2(float(i) / float(N), RadicalInverse_VdC(i));
}

mat3 ImportanceSampleTBN(vec3 Normal)
{
	// from tangent-space to world-space sample vector
	vec3 UpVector = abs(Normal.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
	vec3 Tangent = normalize(cross(UpVector, Normal));
	vec3 Bitangent = cross(Normal, Tangent);
	return mat3(Tangent, Bitangent, Normal);
}

vec3 ImportanceSampleDiffuse(vec2 Xi)
{
	float Phi = Xi.x * TWO_PI;
	float CosTheta = 1.0 - Xi.y;
	float SinTheta = sqrt(1.0 - CosTheta * CosTheta);
	
	// from spherical coordinates to cartesian coordinates - halfway vector
	return vec3(cos(Phi) * SinTheta, sin(Phi) * SinTheta, CosTheta);
}

vec3 ImportanceSampleGGX(vec2 Xi, float AlphaRoughnessSquared)
{
	float Phi = Xi.x * TWO_PI;
	float CosTheta = sqrt((1.0 - Xi.y) / (1.0 + (AlphaRoughnessSquared - 1.0) * Xi.y));
	float SinTheta = sqrt(1.0 - CosTheta * CosTheta);
	
	// from spherical coordinates to cartesian coordinates - halfway vector
	return vec3(cos(Phi) * SinTheta, sin(Phi) * SinTheta, CosTheta);
}