#version 330
uniform sampler2D TextureUnit0; //MainTexture

in vec2 f_UV;

uniform bool FXAAEnabled = true;
uniform vec3 FXAASettings = vec3(0.75, 0.166, 0.0833); //fxaaQualitySubpix, fxaaQualityEdgeThreshold, fxaaQualityEdgeThresholdMin

//const float Exposure = 1.0;

layout(location = 0) out vec3 FragColor;

#define FXAA_GATHER4_ALPHA 0 //Use 1 for Nvidia, 0 - for NVidia and Intel
#define FXAA_QUALITY__PRESET 12 //Default 12, values 10-15, 20-29, 39
#include("FXAA_3_11.glsl")

void main()
{
	vec3 Color;
	if(FXAAEnabled)
	{
		vec2 TextlSize = vec2(1.0) / textureSize(TextureUnit0, 0); // TexelSize = 1 / ScreenSize
		Color = FxaaPixelShader(f_UV, TextureUnit0, TextlSize, FXAASettings.x, FXAASettings.y, FXAASettings.z).rgb;
	}
	else
		Color = texture(TextureUnit0, f_UV).rgb;
	
	FragColor = Color/* * Exposure*/;
}