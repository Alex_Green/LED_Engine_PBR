#version 330
layout(location = 0) in vec3 v_Position;
layout(location = 1) in vec3 v_Normal;
layout(location = 2) in vec2 v_UV;
layout(location = 3) in vec4 v_Tangent;

uniform mat4 MVP;
//uniform mat3 NormalMatrix;
uniform mat4 ModelMatrix;
//uniform mat4 ModelView;

out vec3 f_Position;
out vec2 f_UV;
out mat3 f_TBN; // VectorWS = TBN * VectorTS, WS - World space, TS - Tangent space

void main()
{
	mat4 NormalMatrix = transpose(inverse(ModelMatrix));
	
	vec3 N = normalize(vec3(NormalMatrix * vec4(v_Normal, 0.0)));	// Normal World Space
	//vec3 N = normalize(vec3(ModelMatrix * vec4(v_Normal, 0.0)));		// Normal World Space
	//vec3 T = normalize(NormalMatrix * v_Tangent.xyz);					// Tangent World Space
	vec3 T = normalize(vec3(ModelMatrix * vec4(v_Tangent.xyz, 0.0)));	// Tangent World Space
	vec3 B = normalize(cross(N, T)) * v_Tangent.w;						// Bi-tangent World Space
	f_TBN = mat3(T, B, N);
	
	f_UV = v_UV;
	f_Position = vec3(ModelMatrix * vec4(v_Position, 1.0));
	gl_Position = MVP * vec4(v_Position, 1.0);
}