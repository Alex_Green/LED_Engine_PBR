﻿// https://gitlab.com/Alex_Green/HDRSharpLib
/*                         MIT License
                 Copyright (c) 2018 HDRSharpLib

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Globalization;

namespace HDRSharpLib
{
    #region Enums
    public enum HdrFormat : byte
    {
        RLE_RGBE_32_bits,
        RLE_XYZE_32_bits,
    }

    /// <summary>
    /// The standard coordinate system for an MxN (M - Width, N - Height) picture.
    /// </summary>
    public enum HdrOrientation : byte
    {
        /// <summary>
        /// The standard orientation produced by the renderers, indicating
        /// that Y is decreasing in the file, and X is increasing. X positions
        /// are increasing in each scanline, starting with the upper left
        /// position in the picture and moving to the upper right initially,
        /// then on down the picture. Some programs will only handle
        /// pictures with this ordering.
        /// </summary>
        NegY_PosX,

        /// <summary>
        /// The X ordering has been reversed, effectively flipping the image left to right from the standard ordering.
        /// </summary>
        NegY_NegX,

        /// <summary>
        /// The image has been flipped left to right and also top to bottom, which is the same as rotating it by 180 degrees.
        /// </summary>
        PosY_NegX,

        /// <summary>
        /// The image has been flipped top to bottom from the standard ordering.
        /// </summary>
        PosY_PosX,

        /// <summary>
        /// The image has been rotated 90 degrees clockwise.
        /// </summary>
        PosX_PosY,

        /// <summary>
        /// The image has been rotated 90 degrees clockwise, then flipped top to bottom.
        /// </summary>
        NegX_PosY,

        /// <summary>
        /// The image has been rotated 90 degrees counter-clockwise.
        /// </summary>
        NegX_NegY,

        /// <summary>
        /// The image has been rotate 90 degrees counter-clockwise, then flipped top to bottom.
        /// </summary>
        PosX_NegY
    }
    #endregion

    #region Classes
    public class HdrColor3f : ICloneable
    {
        public float R = 0f;
        public float G = 0f;
        public float B = 0f;

        public HdrColor3f()
        {
        }

        public HdrColor3f(float Value)
        {
            R = G = B = Value;
        }

        public HdrColor3f(float R, float G, float B)
        {
            this.R = R;
            this.G = G;
            this.B = B;
        }

        public HdrColor3f(HdrColor3f ColorCorrection)
        {
            R = ColorCorrection.R;
            G = ColorCorrection.G;
            B = ColorCorrection.B;
        }

        public override string ToString()
        {
            return String.Format("[R: {0}, G: {1}, B: {2}]", R, G, B);
        }

        public override bool Equals(object obj)
        {
            return ((obj is HdrColor3f) ? Equals((HdrColor3f)obj) : false);
        }

        public bool Equals(HdrColor3f item)
        {
            return (R == item.R && G == item.G && B == item.B);
        }

        public static bool operator ==(HdrColor3f item1, HdrColor3f item2)
        {
            if (ReferenceEquals(item1, null))
                return ReferenceEquals(item2, null);

            if (ReferenceEquals(item2, null))
                return ReferenceEquals(item1, null);

            return item1.Equals(item2);
        }

        public static bool operator !=(HdrColor3f item1, HdrColor3f item2)
        {
            return !(item1 == item2);
        }

        public static HdrColor3f operator +(HdrColor3f item1, HdrColor3f item2)
        {
            return new HdrColor3f(item1.R + item2.R, item1.G + item2.G, item1.B + item2.B);
        }

        public static HdrColor3f operator +(HdrColor3f Color3, float f)
        {
            return new HdrColor3f(Color3.R + f, Color3.G + f, Color3.B + f);
        }

        public static HdrColor3f operator +(float f, HdrColor3f Color3)
        {
            return Color3 + f;
        }

        public static HdrColor3f operator -(HdrColor3f item1, HdrColor3f item2)
        {
            return new HdrColor3f(item1.R - item2.R, item1.G - item2.G, item1.B - item2.B);
        }

        public static HdrColor3f operator -(HdrColor3f Color3, float f)
        {
            return new HdrColor3f(Color3.R - f, Color3.G - f, Color3.B - f);
        }

        public static HdrColor3f operator -(float f, HdrColor3f Color3)
        {
            return new HdrColor3f(f - Color3.R, f - Color3.G, f - Color3.B);
        }

        public static HdrColor3f operator *(HdrColor3f item1, HdrColor3f item2)
        {
            return new HdrColor3f(item1.R * item2.R, item1.G * item2.G, item1.B * item2.B);
        }

        public static HdrColor3f operator *(HdrColor3f Color3, float f)
        {
            return new HdrColor3f(Color3.R * f, Color3.G * f, Color3.B * f);
        }

        public static HdrColor3f operator *(float f, HdrColor3f Color3)
        {
            return Color3 * f;
        }

        public static HdrColor3f operator /(HdrColor3f item1, HdrColor3f item2)
        {
            return new HdrColor3f(item1.R / item2.R, item1.G * item2.G, item1.B / item2.B);
        }

        public static HdrColor3f operator /(HdrColor3f Color3, float f)
        {
            return new HdrColor3f(Color3.R / f, Color3.G / f, Color3.B / f);
        }

        public static HdrColor3f operator /(float f, HdrColor3f Color3)
        {
            return new HdrColor3f(f / Color3.R, f / Color3.G, f / Color3.B);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + R.GetHashCode();
                hash = hash * 23 + G.GetHashCode();
                hash = hash * 23 + B.GetHashCode();
                return hash;
            }
        }

        public HdrColor3f Clone()
        {
            return new HdrColor3f(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }

    /// <summary>
    /// The CIE (x,y) chromaticity coordinates
    /// </summary>
    public struct HdrPrimary
    {
        public float X, Y;

        public HdrPrimary(float Value = 0f)
        {
            X = Y = Value;
        }

        public HdrPrimary(float X, float Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public override string ToString()
        {
            return String.Format("[{0}, {1}]", X, Y);
        }

        public override bool Equals(object obj)
        {
            return ((obj is HdrPrimary) ? Equals((HdrPrimary)obj) : false);
        }

        public bool Equals(HdrPrimary item)
        {
            return (X == item.X && Y == item.Y);
        }

        public static bool operator ==(HdrPrimary item1, HdrPrimary item2)
        {
            if (ReferenceEquals(item1, null))
                return ReferenceEquals(item2, null);

            if (ReferenceEquals(item2, null))
                return ReferenceEquals(item1, null);

            return item1.Equals(item2);
        }

        public static bool operator !=(HdrPrimary item1, HdrPrimary item2)
        {
            return !(item1 == item2);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = 17 * 23 + X.GetHashCode();
                return hash * 23 + Y.GetHashCode();
            }
        }
    }

    /// <summary>
    /// The CIE (x,y) chromaticity coordinates of the three (RGB) primaries and the white point used
    /// to standardize the picture's color system. If no PRIMARIES line appears, we assume the standard
    /// primaries "0.640 0.330 0.290 0.600 0.150 0.060 0.333 0.333" for red, green, blue and white.
    /// </summary>
    public class HdrPrimaries : ICloneable
    {
        /// <summary>
        /// CIE (x,y) chromaticity coordinates for Red color.
        /// </summary>
        public HdrPrimary R = new HdrPrimary(0.640f, 0.330f);

        /// <summary>
        /// CIE (x,y) chromaticity coordinates for Green color.
        /// </summary>
        public HdrPrimary G = new HdrPrimary(0.290f, 0.600f);

        /// <summary>
        /// CIE (x,y) chromaticity coordinates for Blue color.
        /// </summary>
        public HdrPrimary B = new HdrPrimary(0.150f, 0.060f);

        /// <summary>
        /// CIE (x,y) chromaticity coordinates for White color.
        /// </summary>
        public HdrPrimary W = new HdrPrimary(0.333f, 0.333f);

        public HdrPrimaries()
        {
        }

        public HdrPrimaries(HdrPrimary R, HdrPrimary G, HdrPrimary B, HdrPrimary W)
        {
            this.R = R;
            this.G = G;
            this.B = B;
            this.W = W;
        }

        public HdrPrimaries(HdrPrimaries primaries)
        {
            R = primaries.R;
            G = primaries.G;
            B = primaries.B;
            W = primaries.W;
        }

        public override string ToString()
        {
            return String.Format("[R: {0}, G: {1}, B: {2}, W: {3}]", R, G, B, W);
        }

        public override bool Equals(object obj)
        {
            return ((obj is HdrPrimaries) ? Equals((HdrPrimaries)obj) : false);
        }

        public bool Equals(HdrPrimaries item)
        {
            return (R == item.R && G == item.G && B == item.B && W == item.W);
        }

        public static bool operator ==(HdrPrimaries item1, HdrPrimaries item2)
        {
            if (ReferenceEquals(item1, null))
                return ReferenceEquals(item2, null);

            if (ReferenceEquals(item2, null))
                return ReferenceEquals(item1, null);

            return item1.Equals(item2);
        }

        public static bool operator !=(HdrPrimaries item1, HdrPrimaries item2)
        {
            return !(item1 == item2);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + R.GetHashCode();
                hash = hash * 23 + G.GetHashCode();
                hash = hash * 23 + B.GetHashCode();
                hash = hash * 23 + W.GetHashCode();
                return hash;
            }
        }

        public HdrPrimaries Clone()
        {
            return new HdrPrimaries(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
    #endregion

    public class HDR : ICloneable
    {
        const string RADIANCE_Signature = "#?RADIANCE";

        public HdrFormat Format = HdrFormat.RLE_RGBE_32_bits;
        public HdrColor3f ColorCorrection = null;
        public string Software = null;
        public float PixelAspectRatio = float.NaN;
        public string View = null;
        public HdrPrimaries Primaries = null;
        public List<string> HeaderComments = new List<string>();
        public List<string> OtherHeaderRecords = new List<string>();
        public HdrOrientation Orientation = HdrOrientation.NegY_PosX;
        public float[] ImageData = new float[0];

        #region HDR Creation, Loading, Saving...
        /// <summary>
        /// Create new empty <see cref="HDR"/> istance.
        /// </summary>
        public HDR()
        {
        }

        /// <summary>
        /// Create <see cref="HDR"/> instance with some params.
        /// </summary>
        /// <param name="Width">Image Width.</param>
        /// <param name="Height">Image Height.</param>
        /// <param name="Format">Image Type (is RLE compressed RGBE or XYZE image).</param>
        public HDR(int Width, int Height)
        {
            this.Width = Math.Max(0, Width);
            this.Height = Math.Max(0, Height);
            ImageData = new float[Width * Height * 3];
        }

        /// <summary>
        /// Make <see cref="HDR"/> from some <see cref="HDR"/> instance.
        /// Equal to <see cref="HDR.Clone()"/> function.
        /// </summary>
        /// <param name="image">Original <see cref="HDR"/> instance.</param>
        public HDR(HDR image)
        {
            Format = image.Format;
            Exposure = image.Exposure;
            Gamma = image.Gamma;
            ColorCorrection = image.ColorCorrection.Clone();
            Software = image.Software;
            PixelAspectRatio = image.PixelAspectRatio;
            View = image.View;
            Primaries = image.Primaries.Clone();
            HeaderComments = new List<string>(image.HeaderComments);
            OtherHeaderRecords = new List<string>(image.OtherHeaderRecords);

            Orientation = image.Orientation;
            Width = image.Width;
            Height = image.Height;

            ImageData = new float[image.ImageData.LongLength];
            Array.Copy(image.ImageData, ImageData, image.ImageData.Length);
        }

        /// <summary>
        /// Load <see cref="HDR"/> from file.
        /// </summary>
        /// <param name="filename">Full path to file.</param>
        /// <returns>Loaded <see cref="HDR"/> file.</returns>
        public HDR(string filename)
        {
            LoadFunc(filename);
        }

        /// <summary>
        /// Make <see cref="HDR"/> from bytes array.
        /// </summary>
        /// <param name="bytes">Bytes array (same like File).</param>
        public HDR(byte[] bytes)
        {
            LoadFunc(bytes);
        }

        /// <summary>
        /// Convert <see cref="Bitmap"/> to <see cref="HDR"/>.
        /// Formula: (Pixel ^ Gamma + Offset) * Exposure.
        /// </summary>
        /// <param name="BMP">Input <see cref="Bitmap"/> image.</param>
        /// <param name="Exposure">Exposure value, default 1.0 (like in Photoshop, but not in percents).</param>
        /// <param name="Offset">Offset value, default 0.0.</param>
        /// <param name="Gamma">Gama value, default it's 2.2 or 1.0</param>
        /// <returns><see cref="HDR"/> or null, on error.</returns>
        public HDR(Bitmap BMP, float Exposure = 1f, float Offset = 0f, float Gamma = 2.2f)
        {
            try
            {
                if (BMP == null)
                    throw new ArgumentNullException("Input Bitmap image is null.");

                PixelFormat PixFormat = BMP.PixelFormat;

                // Bitmap width must by aligned (align value = 32 bits = 4 bytes)!
                int StrideBytes = BMP.Width * (Image.GetPixelFormatSize(PixFormat) >> 3);
                int PaddingBytes = (int)Math.Ceiling(StrideBytes / 4.0) * 4 - StrideBytes;

                byte[] ImgData = new byte[(StrideBytes + PaddingBytes) * BMP.Height];
                BitmapData BmpData = BMP.LockBits(new Rectangle(0, 0, BMP.Width, BMP.Height), ImageLockMode.ReadOnly, BMP.PixelFormat);
                Marshal.Copy(BmpData.Scan0, ImgData, 0, ImgData.Length);
                BMP.UnlockBits(BmpData);
                BmpData = null;

                Width = Math.Max(0, BMP.Width);
                Height = Math.Max(0, BMP.Height);
                ImageData = new float[Width * Height * 3];

                byte[] Data = new byte[ImageData.LongLength];

                if (PaddingBytes > 0) // BMP has align
                    for (long i = 0; i < BMP.Height; i++)
                        Array.Copy(ImgData, i * (StrideBytes + PaddingBytes), Data, i * StrideBytes, StrideBytes);
                else
                    Data = ImgData;

                const double OneDiv255 = 1.0 / 255.0;
                for (long i = 0; i < Data.LongLength; i += 3)
                {
                    float B = (float)(Math.Pow(Data[i] * OneDiv255, Gamma) + Offset) * Exposure;
                    float G = (float)(Math.Pow(Data[i + 1] * OneDiv255, Gamma) + Offset) * Exposure;
                    float R = (float)(Math.Pow(Data[i + 2] * OneDiv255, Gamma) + Offset) * Exposure;

                    ImageData[i] = R;
                    ImageData[i + 1] = G;
                    ImageData[i + 2] = B;
                }
                ImgData = null;
                Data = null;
            }
            catch
            {
                throw new Exception("HDR from Bitmap creation error.");
            }
        }

        /// <summary>
        /// Make <see cref="HDR"/> from <see cref="Stream"/>.
        /// For file opening better use <see cref="FromFile(string)"/>.
        /// </summary>
        /// <param name="stream">Some stream. You can use a lot of Stream types, but Stream must support:
        /// <see cref="Stream.CanSeek"/> and <see cref="Stream.CanRead"/>.</param>
        public HDR(Stream stream)
        {
            LoadFunc(stream);
        }

        /// <summary>
        /// Load <see cref="HDR"/> from file.
        /// </summary>
        /// <param name="filename">Full path to file.</param>
        /// <returns>Loaded <see cref="HDR"/> file.</returns>
        public static HDR FromFile(string filename)
        {
            return new HDR(filename);
        }

        /// <summary>
        /// Make <see cref="HDR"/> from bytes array.
        /// </summary>
        /// <param name="bytes">Bytes array (same like File).</param>
        public static HDR FromBytes(byte[] bytes)
        {
            return new HDR(bytes);
        }

        /// <summary>
        /// Make <see cref="HDR"/> from <see cref="Stream"/>.
        /// For file opening better use <see cref="FromFile(string)"/>.
        /// </summary>
        /// <param name="stream">Some stream. You can use a lot of Stream types, but Stream must support:
        /// <see cref="Stream.CanSeek"/> and <see cref="Stream.CanRead"/>.</param>
        public static HDR FromStream(Stream stream)
        {
            return new HDR(stream);
        }

        /// <summary>
        /// Save <see cref="HDR"/> to file.
        /// </summary>
        /// <param name="filename">Full path to file.</param>
        /// <returns>Return "true", if all done or "false", if failed.</returns>
        public bool Save(string filename)
        {
            try
            {
                bool Result = false;
                using (FileStream Fs = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (MemoryStream Ms = new MemoryStream())
                    {
                        Result = SaveFunc(Ms);
                        Ms.WriteTo(Fs);
                        Fs.Flush();
                    }
                }
                return Result;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Save <see cref="HDR"/> to <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">Some stream, it must support: <see cref="Stream.CanWrite"/>.</param>
        /// <returns>Return "true", if all done or "false", if failed.</returns>
        public bool Save(Stream stream)
        {
            return SaveFunc(stream);
        }

        /// <summary>
        /// Apply Exposure, Offset and Gamma correction.
        /// </summary>
        /// <param name="Exposure">Exposure value, default 1.0 (like in Photoshop, but not in percents).</param>
        /// <param name="Offset">Offset value, default 0.0.</param>
        /// <param name="Gamma">Gama value (inverted), default 1/2.2 or 1.0</param>
        public void Correction(float Exposure = 1f, float Offset = 0f, float Gamma = 1.0f)
        {
            for (long i = 0; i < ImageData.LongLength; i++)
                ImageData[i] = ((float)Math.Pow(ImageData[i], Gamma) + Offset) * Exposure;
        }
        #endregion

        /// <summary>
        /// Indicating a multiplier that has been applied to all the pixels in the file.
        /// EXPOSURE values are cumulative, so the original pixel values must be derived by taking
        /// the values in the file and dividing by all the EXPOSURE settings multiplied together.
        /// </summary>
        public float Exposure { get; set; } = float.NaN;

        /// <summary>
        /// Indicating a gamma value, not explaned at original format info file!
        /// </summary>
        public float Gamma { get; set; } = float.NaN;

        /// <summary>
        /// Gets or Sets image Width value in "Resolution String" section after "Information Header" section.
        /// </summary>
        public int Width { get; set; } = 0;

        /// <summary>
        /// Gets or Sets image Height value in "Resolution String" section after "Information Header" section.
        /// </summary>
        public int Height { get; set; } = 0;

        /// <summary>
        /// Gets or Sets image Size.
        /// </summary>
        public Size Size
        {
            get { return new Size(Width, Height); }
            set
            {
                Width = value.Width;
                Height = value.Height;
            }
        }

        /// <summary>
        /// Make full independed copy of <see cref="HDR"/>.
        /// </summary>
        /// <returns>Full independed copy of <see cref="HDR"/>.</returns>
        public HDR Clone()
        {
            return new HDR(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        /// <summary>
        /// Get information from image.
        /// </summary>
        /// <returns>MultiLine string with info fields (one per line).</returns>
        public string GetInfo()
        {
            StringBuilder SB = new StringBuilder();

            SB.AppendLine("Width: " + Width);
            SB.AppendLine("Height: " + Height);
            SB.AppendLine("Orientation: " + Orientation);
            SB.AppendLine("Format: " + Format);

            if (!float.IsNaN(Exposure))
                SB.AppendLine("Exposure: " + Exposure);

            if (!float.IsNaN(Gamma))
                SB.AppendLine("Gamma: " + Gamma);

            if (ColorCorrection != null)
                SB.AppendLine("Color correction: " + ColorCorrection);

            if (Software != null)
                SB.AppendLine("Software: " + Software);

            if (!float.IsNaN(PixelAspectRatio))
                SB.AppendLine("Pixel aspect ratio: " + PixelAspectRatio);

            if (View != null)
                SB.AppendLine("View: " + View);

            if (Primaries != null)
                SB.AppendLine("Primaries: " + Primaries);

            return SB.ToString();
        }

        #region Convert
        /// <summary>
        /// Convert <see cref="HDR"/> to <see cref="Bitmap"/>.
        /// Formula: (Pixel * Exposure + Offset) ^ Gamma.
        /// </summary>
        /// <param name="Exposure">Exposure value, default 1.0 (like in Photoshop, but not in percents).</param>
        /// <param name="Offset">Offset value, default 0.0.</param>
        /// <param name="Gamma">Gama value (inverted), default 1/2.2 or 1.0</param>
        /// <returns><see cref="Bitmap"/> or null, on error.</returns>
        public Bitmap ToBitmap(float Exposure = 1f, float Offset = 0f, float Gamma = 1f / 2.2f)
        {
            try
            {
                byte[] Data = new byte[ImageData.LongLength];
                for (long i = 0; i < Data.LongLength; i += 3)
                {
                    float R = ImageData[i];
                    float G = ImageData[i + 1];
                    float B = ImageData[i + 2];

                    Data[i] = (byte)(Math.Min(Math.Max((float)Math.Pow(B * Exposure + Offset, Gamma) * 255f + 0.5f, 0f), 255f));
                    Data[i + 1] = (byte)(Math.Min(Math.Max((float)Math.Pow(G * Exposure + Offset, Gamma) * 255f + 0.5f, 0f), 255f));
                    Data[i + 2] = (byte)(Math.Min(Math.Max((float)Math.Pow(R * Exposure + Offset, Gamma) * 255f + 0.5f, 0f), 255f));
                }

                Bitmap BMP = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);

                // Bitmap width must by aligned (align value = 32 bits = 4 bytes)!
                int StrideBytes = BMP.Width * 3;
                int PaddingBytes = (int)Math.Ceiling(StrideBytes / 4.0) * 4 - StrideBytes;

                byte[] ImgData = new byte[(StrideBytes + PaddingBytes) * BMP.Height];
                if (PaddingBytes > 0) //Need bytes align
                    for (long i = 0; i < BMP.Height; i++)
                        Array.Copy(Data, i * StrideBytes, ImgData, i * (StrideBytes + PaddingBytes), StrideBytes);
                else
                    ImgData = Data;

                BitmapData BmpData = BMP.LockBits(new Rectangle(0, 0, BMP.Width, BMP.Height), ImageLockMode.WriteOnly, BMP.PixelFormat);
                Marshal.Copy(ImgData, 0, BmpData.Scan0, ImgData.Length);
                BMP.UnlockBits(BmpData);
                ImgData = null;
                Data = null;
                BmpData = null;

                #region Flip Image
                switch (Orientation)
                {
                    default:
                    case HdrOrientation.NegY_PosX:
                        break;

                    case HdrOrientation.NegY_NegX:
                        BMP.RotateFlip(RotateFlipType.RotateNoneFlipX);
                        break;
                    case HdrOrientation.PosY_NegX:
                        BMP.RotateFlip(RotateFlipType.RotateNoneFlipXY);
                        break;
                    case HdrOrientation.PosY_PosX:
                        BMP.RotateFlip(RotateFlipType.RotateNoneFlipY);
                        break;
                    case HdrOrientation.PosX_PosY:
                        BMP.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        break;
                    case HdrOrientation.NegX_PosY:
                        BMP.RotateFlip(RotateFlipType.Rotate90FlipY);
                        break;
                    case HdrOrientation.NegX_NegY:
                        BMP.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        break;
                    case HdrOrientation.PosX_NegY:
                        BMP.RotateFlip(RotateFlipType.Rotate270FlipY);
                        break;
                }
                #endregion

                return BMP;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Convert <see cref="HDR"/> to bytes array.
        /// </summary>
        /// <returns>Bytes array, (equal to saved file, but in memory) or null (on error).</returns>
        public byte[] ToBytes()
        {
            try
            {
                byte[] Bytes;
                using (MemoryStream ms = new MemoryStream())
                {
                    Save(ms);
                    Bytes = ms.ToArray();
                    ms.Flush();
                }
                return Bytes;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Convert <see cref="Bitmap"/> to <see cref="HDR"/>.
        /// </summary>
        /// <param name="BMP">Input <see cref="Bitmap"/> image.</param>
        /// <param name="Exposure">Exposure value, default 1.0.</param>
        /// <param name="Offset">Offset value, default 0.0.</param>
        /// <param name="Gamma">Gama value, default it's 2.2 or 1.0</param>
        /// <returns><see cref="HDR"/> or null, on error.</returns>
        public static HDR FromBitmap(Bitmap BMP, float Exposure = 1f, float Offset = 0f, float Gamma = 2.2f)
        {
            return new HDR(BMP, Exposure, Offset, Gamma);
        }
        #endregion

        #region Private functions
        void LoadFunc(string filename)
        {
            if (!File.Exists(filename))
                throw new FileNotFoundException("File: \"" + filename + "\" not found!");

            using (FileStream FS = new FileStream(filename, FileMode.Open))
                LoadFunc(FS);
        }

        void LoadFunc(byte[] bytes)
        {
            if (bytes == null)
                throw new ArgumentNullException();

            using (MemoryStream FS = new MemoryStream(bytes, false))
                LoadFunc(FS);
        }

        void LoadFunc(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException();
            if (!(stream.CanRead && stream.CanSeek))
                throw new FileLoadException("Stream reading or seeking is not avaiable!");

            try
            {
                stream.Seek(0, SeekOrigin.Begin);
                BinaryReader Br = new BinaryReader(stream);

                if (Encoding.ASCII.GetString(Br.ReadBytes(RADIANCE_Signature.Length)) != RADIANCE_Signature)
                    throw new Exception("Can't found \"" + RADIANCE_Signature + "\" signature.");

                // Get end of the header: "0x0A 0x0A" and start of image data (next "0x0A" after end of the header)
                byte Byte1 = byte.MinValue; // First byte
                byte Byte2 = byte.MinValue; // Second byte
                do
                {
                    if (stream.Position < stream.Length)
                    {
                        Byte1 = Byte2;
                        Byte2 = Br.ReadByte();
                    }
                    else
                        throw new Exception("File has wrong format: no header found.");
                }
                while (Byte1 != 0x0A || Byte1 != Byte2);

                do
                {
                    if (stream.Position < stream.Length)
                        Byte1 = Br.ReadByte();
                    else
                        throw new Exception("File has wrong format: no coords found.");
                }
                while (Byte1 != 0x0A);

                int ImageDataOffset = (int)stream.Position - 1;

                #region Parse Header
                stream.Seek(0, SeekOrigin.Begin);
                string Header = Encoding.ASCII.GetString(Br.ReadBytes(ImageDataOffset));

                string[] HeaderLines = Header.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < HeaderLines.Length; i++)
                {
                    string L = HeaderLines[i].Trim();

                    if (L.Length <= 0)
                    {
                        continue;
                    }
                    if (L[0] == '#') // Header comment
                    {
                        if (L != RADIANCE_Signature)
                            HeaderComments.Add(L);
                    }
                    else if (L[0] == '-' || L[0] == '+') // Coords: Width, Height, Rotation, Flipping
                    {
                        try
                        {
                            string[] Coords = L.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            switch (Coords[0] + Coords[2])
                            {
                                case "-X-Y":
                                    Orientation = HdrOrientation.NegX_NegY;
                                    break;

                                case "-X+Y":
                                    Orientation = HdrOrientation.NegX_PosY;
                                    break;

                                case "-Y-X":
                                    Orientation = HdrOrientation.NegY_NegX;
                                    break;

                                default:
                                case "-Y+X":
                                    Orientation = HdrOrientation.NegY_PosX;
                                    break;

                                case "+X-Y":
                                    Orientation = HdrOrientation.PosX_NegY;
                                    break;

                                case "+X+Y":
                                    Orientation = HdrOrientation.PosX_PosY;
                                    break;

                                case "+Y-X":
                                    Orientation = HdrOrientation.PosY_NegX;
                                    break;

                                case "+Y+X":
                                    Orientation = HdrOrientation.PosY_PosX;
                                    break;
                            }

                            switch (Orientation)
                            {
                                default:
                                case HdrOrientation.NegY_NegX:
                                case HdrOrientation.NegY_PosX:
                                case HdrOrientation.PosY_NegX:
                                case HdrOrientation.PosY_PosX:
                                    Width = int.Parse(Coords[3]);
                                    Height = int.Parse(Coords[1]);
                                    break;

                                case HdrOrientation.NegX_NegY:
                                case HdrOrientation.NegX_PosY:
                                case HdrOrientation.PosX_NegY:
                                case HdrOrientation.PosX_PosY:
                                    Width = int.Parse(Coords[1]);
                                    Height = int.Parse(Coords[3]);
                                    break;
                            }
                        }
                        catch
                        {
                            throw new Exception("Can't read file coords/size");
                        }

                    }
                    else // Header params
                    {
                        string[] S = L.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                        if (S.Length < 3 || S[1].Trim() != "=")
                            continue;

                        string S_Name = S[0].Trim();
                        string S_Value = S[2].Trim();
                        switch (S_Name)
                        {
                            default:
                                OtherHeaderRecords.Add(L);
                                break;

                            case "FORMAT":
                                if (S_Value == "32-bit_rle_rgbe")
                                {
                                    Format = HdrFormat.RLE_RGBE_32_bits;
                                }
                                else if (S_Value == "32-bit_rle_xyze")
                                {
                                    Format = HdrFormat.RLE_XYZE_32_bits;
                                }
                                else
                                    throw new Exception("Format is unsupported.");
                                break;

                            case "GAMMA":
                                try
                                {
                                    Gamma = float.Parse(S_Value, CultureInfo.InvariantCulture);
                                }
                                catch
                                {
                                    throw new Exception("Gamma parse error.");
                                }
                                break;

                            case "EXPOSURE":
                                try
                                {
                                    Exposure = float.Parse(S_Value, CultureInfo.InvariantCulture);
                                }
                                catch
                                {
                                    throw new Exception("Exposure parse error.");
                                }
                                break;

                            case "COLORCORR":
                                try
                                {
                                    string[] RGB = S_Value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                    ColorCorrection = new HdrColor3f(
                                        float.Parse(RGB[0], CultureInfo.InvariantCulture),
                                        float.Parse(RGB[1], CultureInfo.InvariantCulture),
                                        float.Parse(RGB[2], CultureInfo.InvariantCulture));
                                }
                                catch
                                {
                                    throw new Exception("ColorCorrection parse error.");
                                }
                                break;

                            case "SOFTWARE":
                                Software = S_Value;
                                break;

                            case "PIXASPECT":
                                PixelAspectRatio = float.Parse(S_Value, CultureInfo.InvariantCulture);
                                break;

                            case "VIEW":
                                View = S_Value;
                                break;

                            case "PRIMARIES":
                                try
                                {
                                    string[] RGBW = S_Value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                    HdrPrimary PrimaryR = new HdrPrimary(float.Parse(RGBW[0], CultureInfo.InvariantCulture), float.Parse(RGBW[1], CultureInfo.InvariantCulture));
                                    HdrPrimary PrimaryG = new HdrPrimary(float.Parse(RGBW[2], CultureInfo.InvariantCulture), float.Parse(RGBW[3], CultureInfo.InvariantCulture));
                                    HdrPrimary PrimaryB = new HdrPrimary(float.Parse(RGBW[4], CultureInfo.InvariantCulture), float.Parse(RGBW[5], CultureInfo.InvariantCulture));
                                    HdrPrimary PrimaryW = new HdrPrimary(float.Parse(RGBW[6], CultureInfo.InvariantCulture), float.Parse(RGBW[7], CultureInfo.InvariantCulture));
                                    Primaries = new HdrPrimaries(PrimaryR, PrimaryG, PrimaryB, PrimaryW);
                                }
                                catch
                                {
                                    throw new Exception("ColorCorrection parse error.");
                                }
                                break;
                        }
                    }
                }
                #endregion

                stream.Seek(1, SeekOrigin.Current);
                byte[] ImgData = Br.ReadBytes((int)stream.Length - ImageDataOffset - 1);
                Br.Close();

                #region Decode Data
                if (ImgData.LongLength == 0)
                    return;

                if (Width < 8 || Width > 0x7fff || // RLE is not allowed so read flat
                    ImgData[0] != 0x02 || ImgData[1] != 0x02 || ImgData[2] >= 128) // not run length encoded
                    ImageData = DecodeRGBE(ImgData);
                else // Decode RLE
                {
                    byte[] DataDecoded = new byte[Width * Height * 4];

                    #region RLE Decode
                    // Based on ThreeJS (MIT License)

                    int Scanlines_Count = Height;
                    long Offset = 0;
                    long Pos = 0;
                    int Ptr = 0;
                    int Ptr_End = Width * 4;
                    byte[] RGBE = new byte[4];
                    byte[] Scanline_Buffer = new byte[Ptr_End];

                    // read in each successive scanline
                    while (Scanlines_Count > 0 && Pos < ImgData.LongLength)
                    {
                        if (Pos + 4 > ImgData.LongLength)
                            throw new IndexOutOfRangeException("Image reading error. Pos > image data length.");

                        RGBE[0] = ImgData[Pos++];
                        RGBE[1] = ImgData[Pos++];
                        RGBE[2] = ImgData[Pos++];
                        RGBE[3] = ImgData[Pos++];

                        if (2 != RGBE[0] || 2 != RGBE[1] || ((RGBE[2] << 8) | RGBE[3]) != Width)
                            throw new Exception("RLE: Wrong Scanline Width.");

                        // Read each of the four channels for the scanline into the buffer.
                        // First red, then green, then blue, then exponent.
                        Ptr = 0;
                        while ((Ptr < Ptr_End) && (Pos < ImgData.LongLength))
                        {
                            int Count = ImgData[Pos++];
                            bool IsRLE = Count > 128;
                            if (IsRLE)
                                Count -= 128;

                            if (Count == 0 || Ptr + Count > Ptr_End)
                                throw new Exception("RLE: Wrong Scanline Data.");

                            if (IsRLE)
                            {
                                byte B = ImgData[Pos++];
                                for (int i = 0; i < Count; i++)
                                    Scanline_Buffer[Ptr++] = B;
                            }
                            else // A literal-run
                            {
                                Array.Copy(ImgData, Pos, Scanline_Buffer, Ptr, Count);
                                Ptr += Count;
                                Pos += Count;
                            }
                        }

                        // Now convert data from buffer into rgba
                        // first red, then green, then blue, then exponent (alpha)
                        for (int i = 0; i < Width; i++)
                        {
                            long ScanLine_Buffer_Offset = 0;
                            DataDecoded[Offset] = Scanline_Buffer[i + ScanLine_Buffer_Offset];

                            ScanLine_Buffer_Offset += Width;
                            DataDecoded[Offset + 1] = Scanline_Buffer[i + ScanLine_Buffer_Offset];

                            ScanLine_Buffer_Offset += Width;
                            DataDecoded[Offset + 2] = Scanline_Buffer[i + ScanLine_Buffer_Offset];

                            ScanLine_Buffer_Offset += Width;
                            DataDecoded[Offset + 3] = Scanline_Buffer[i + ScanLine_Buffer_Offset];
                            Offset += 4;
                        }
                        Scanlines_Count--;
                    }
                    #endregion

                    ImageData = DecodeRGBE(DataDecoded);
                }
                #endregion

                ImgData = null;
            }
            catch
            {
                throw new Exception("Loading error");
            }
        }

        float[] DecodeRGBE(byte[] Data)
        {
            if (Data == null)
                throw new ArgumentNullException("DecodeRGBE error.");

            float[] FData = new float[Width * Height * 3];

            for (long i = 0; i < Width * Height; i++)
            {
                long I3 = i * 3;
                long I4 = i * 4;
                int E = Data[I4 + 3];

                HdrColor3f RGB16 = new HdrColor3f();
                if (E > 0)
                {
                    float f = (float)Math.Pow(2, E - 136); // 136 = 128 + 8 (Equal to 2^(E-128) / 256)
                    RGB16.R = (Data[I4] + 0.5f) * f;
                    RGB16.G = (Data[I4 + 1] + 0.5f) * f;
                    RGB16.B = (Data[I4 + 2] + 0.5f) * f;
                }
                /*
                if (ColorCorrection != null)
                    RGB16 *= ColorCorrection;

                if (!float.IsNaN(Exposure))
                {
                    RGB16.R = Exposure * (float)Math.Pow(2, RGB16.R);
                    RGB16.G = Exposure * (float)Math.Pow(2, RGB16.G);
                    RGB16.B = Exposure * (float)Math.Pow(2, RGB16.B);
                }

                if (!float.IsNaN(Gamma))
                {
                    RGB16.R = (float)Math.Pow(RGB16.R, Gamma);
                    RGB16.G = (float)Math.Pow(RGB16.G, Gamma);
                    RGB16.B = (float)Math.Pow(RGB16.B, Gamma);
                }
                */

                FData[I3] = RGB16.R;
                FData[I3 + 1] = RGB16.G;
                FData[I3 + 2] = RGB16.B;
            }

            return FData;
        }

        #region frexp math functions from C.math.NET Library (https://github.com/MachineCognitis/C.math.NET)
        double frexp(double number, out int exponent)
        {
            const long DBL_EXP_MASK = 0x7ff0000000000000L;
            const int DBL_MANT_BITS = 52;
            const long DBL_EXP_CLR_MASK = unchecked((long)0X800FFFFFFFFFFFFFL);

            long bits = BitConverter.DoubleToInt64Bits(number);
            int exp = (int)((bits & DBL_EXP_MASK) >> DBL_MANT_BITS);
            exponent = 0;

            if (exp == 0x7ff || number == 0D)
                number += number;
            else
            {
                // Not zero and finite.
                exponent = exp - 1022;
                if (exp == 0)
                {
                    // Subnormal, scale number so that it is in [1, 2).
                    number *= BitConverter.Int64BitsToDouble(0x4350000000000000L); // 2^54
                    bits = BitConverter.DoubleToInt64Bits(number);
                    exp = (int)((bits & DBL_EXP_MASK) >> DBL_MANT_BITS);
                    exponent = exp - 1022 - 54;
                }
                // Set exponent to -1 so that number is in [0.5, 1).
                number = BitConverter.Int64BitsToDouble((bits & DBL_EXP_CLR_MASK) | 0x3fe0000000000000L);
            }

            return number;
        }

        float frexp(float number, out int exponent)
        {
            const int FLT_EXP_MASK = 0X7F800000;
            const int FLT_MANT_BITS = 23;
            const int FLT_EXP_CLR_MASK = unchecked((int)0X807FFFFF);

            int bits = BitConverter.ToInt32(BitConverter.GetBytes(number), 0);
            int exp = (bits & FLT_EXP_MASK) >> FLT_MANT_BITS;
            exponent = 0;

            if (exp == 0xff || number == 0F)
                number += number;
            else
            {
                // Not zero and finite.
                exponent = exp - 126;
                if (exp == 0)
                {
                    // Subnormal, scale number so that it is in [1, 2).
                    number *= BitConverter.ToSingle(BitConverter.GetBytes(0x4c000000), 0); // 2^25
                    bits = BitConverter.ToInt32(BitConverter.GetBytes(number), 0);
                    exp = (bits & FLT_EXP_MASK) >> FLT_MANT_BITS;
                    exponent = exp - 126 - 25;
                }
                // Set exponent to -1 so that number is in [0.5, 1).
                number = BitConverter.ToSingle(BitConverter.GetBytes((bits & FLT_EXP_CLR_MASK) | 0x3f000000), 0);
            }
            return number;
        }
        #endregion

        bool SaveFunc(Stream stream)
        {
            try
            {
                if (stream == null)
                    throw new ArgumentNullException();
                if (!(stream.CanWrite && stream.CanSeek))
                    throw new FileLoadException("Stream writing or seeking is not avaiable!");

                BinaryWriter Bw = new BinaryWriter(stream);

                #region Save Header
                Bw.Write(Encoding.ASCII.GetBytes(RADIANCE_Signature));
                Bw.Write((byte)0x0A);

                if (HeaderComments != null)
                {
                    for (int i = 0; i < HeaderComments.Count; i++)
                    {
                        Bw.Write(Encoding.ASCII.GetBytes(HeaderComments[i]));
                        Bw.Write((byte)0x0A);
                    }
                }

                if (Primaries != null)
                {
                    Bw.Write(Encoding.ASCII.GetBytes(String.Format("={0} {1} {2} {3} {4} {5} {6} {7}",
                        Primaries.R.X, Primaries.R.Y,
                        Primaries.G.X, Primaries.G.Y,
                        Primaries.B.X, Primaries.B.Y,
                        Primaries.W.X, Primaries.W.Y)));
                    Bw.Write((byte)0x0A);
                }

                Bw.Write(Encoding.ASCII.GetBytes("FORMAT=" + (Format == HdrFormat.RLE_RGBE_32_bits ? "32-bit_rle_rgbe" : "32-bit_rle_xyze")));
                Bw.Write((byte)0x0A);

                if (!float.IsNaN(Gamma))
                {
                    Bw.Write(Encoding.ASCII.GetBytes("GAMMA=" + Gamma.ToString()));
                    Bw.Write((byte)0x0A);
                }

                if (!float.IsNaN(Exposure))
                {
                    Bw.Write(Encoding.ASCII.GetBytes("EXPOSURE=" + Exposure.ToString()));
                    Bw.Write((byte)0x0A);
                }

                if (ColorCorrection != null)
                {
                    Bw.Write(Encoding.ASCII.GetBytes(String.Format("COLORCORR={0} {1} {2}", ColorCorrection.R, ColorCorrection.G, ColorCorrection.B)));
                    Bw.Write((byte)0x0A);
                }

                if (Software != null)
                {
                    Bw.Write(Encoding.ASCII.GetBytes("SOFTWARE=" + Software));
                    Bw.Write((byte)0x0A);
                }

                if (!float.IsNaN(PixelAspectRatio))
                {
                    Bw.Write(Encoding.ASCII.GetBytes("PIXASPECT=" + PixelAspectRatio));
                    Bw.Write((byte)0x0A);
                }

                if (View != null)
                {
                    Bw.Write(Encoding.ASCII.GetBytes("VIEW=" + View));
                    Bw.Write((byte)0x0A);
                }

                Bw.Write((byte)0x0A); // End of header

                string OrientationStr;
                switch (Orientation)
                {
                    default:
                    case HdrOrientation.NegY_PosX:
                        OrientationStr = String.Format("-Y {0} +X {1}", Height, Width);
                        break;
                    case HdrOrientation.NegX_NegY:
                        OrientationStr = String.Format("-X {0} -Y {1}", Width, Height);
                        break;
                    case HdrOrientation.NegX_PosY:
                        OrientationStr = String.Format("-X {0} +Y {1}", Width, Height);
                        break;
                    case HdrOrientation.NegY_NegX:
                        OrientationStr = String.Format("-Y {0} -X {1}", Height, Width);
                        break;
                    case HdrOrientation.PosX_NegY:
                        OrientationStr = String.Format("+X {0} -Y {1}", Width, Height);
                        break;
                    case HdrOrientation.PosX_PosY:
                        OrientationStr = String.Format("+X {0} +Y {1}", Width, Height);
                        break;
                    case HdrOrientation.PosY_NegX:
                        OrientationStr = String.Format("+Y {0} -X {1}", Height, Width);
                        break;
                    case HdrOrientation.PosY_PosX:
                        OrientationStr = String.Format("+Y {0} +X {1}", Height, Width);
                        break;
                }
                Bw.Write(Encoding.ASCII.GetBytes(OrientationStr));
                Bw.Write((byte)0x0A);
                #endregion

                #region Save image Data
                if (ImageData != null)
                {
                    byte[] RGBE_Data = new byte[Width * Height * 4];
                    for (long i = 0; i < Width * Height; i++)
                    {
                        long I3 = i * 3;
                        long I4 = i * 4;

                        float R = ImageData[I3];
                        float G = ImageData[I3 + 1];
                        float B = ImageData[I3 + 2];

                        double V = (G > R ? G : R);
                        if (B > V)
                            V = B;

                        if (V < 1e-32)
                            RGBE_Data[I4] = RGBE_Data[I4 + 1] = RGBE_Data[I4 + 2] = RGBE_Data[I4 + 3] = 0;
                        else
                        {
                            int e;
                            V = frexp(V, out e) * 255.9999 / V;
                            RGBE_Data[I4] = (byte)(R * V);
                            RGBE_Data[I4 + 1] = (byte)(G * V);
                            RGBE_Data[I4 + 2] = (byte)(B * V);
                            RGBE_Data[I4 + 3] = (byte)(e + 128);
                        }
                    }

                    bool UseRLE = false; // TODO
                    if (UseRLE)
                    {
                        throw new NotImplementedException(); // TODO
                    }
                    else
                        Bw.Write(RGBE_Data);
                }
                #endregion

                Bw.Flush();
                stream.Flush();

                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Explicit
        public static explicit operator Bitmap(HDR image)
        {
            return image.ToBitmap();
        }

        public static explicit operator HDR(Bitmap bmp)
        {
            return FromBitmap(bmp);
        }
        #endregion
    }
}
