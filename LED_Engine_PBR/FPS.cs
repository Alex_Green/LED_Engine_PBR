﻿using System;
using System.Collections.Generic;
//using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using QuickFont;
using QuickFont.Configuration;

namespace LED_Engine_PBR
{
    public static class FPS
    {
        const int ElapsedMax = 20; //Calc FPS every 'ElapsedMax' frame

        public static bool ShowFPS = false;
        public static double RealFPS = 60.0; //Start value

        static int ElapsedIndex = 0;
        static double[] Elapsed = new double[ElapsedMax];

        #region FPS Font
        static QFont FPS_Font;
        public static QFontDrawing FPS_FontDrawing;
        public static Matrix4 FPS_Font_ProjectionMatrix = Matrix4.Identity;
        static QFontRenderOptions FPS_Font_Options;

        public static void Font_Init()
        {
            Font_Free();

            FPS_FontDrawing = new QFontDrawing();

            var FontBuilderConfig = new QFontBuilderConfiguration(true);
            FontBuilderConfig.ShadowConfig.blurRadius = 2;
            FontBuilderConfig.ShadowConfig.blurPasses = 1;
            FontBuilderConfig.ShadowConfig.Type = ShadowType.Blurred;
            //FontBuilderConfig.TextGenerationRenderHint = TextGenerationRenderHint.ClearTypeGridFit;

            string FPS_FontPath = Engine.CombinePaths(Settings.Paths.EngineFonts, @"LCD\LCD.ttf");
            FPS_Font = new QFont(FPS_FontPath, 24f, FontBuilderConfig);

            FPS_Font_Options = new QFontRenderOptions();
            FPS_Font_Options.DropShadowActive = true;
            FPS_Font_Options.Colour = Color.Yellow;
        }

        public static void Font_Free()
        {
            if (FPS_Font != null)
                FPS_Font.Dispose();
            if (FPS_FontDrawing != null)
                FPS_FontDrawing.Dispose();
        }

        public static void Font_Render()
        {
            string FPS_RenderStr = RealFPS.ToString("0.0");

            FPS_FontDrawing.ProjectionMatrix = FPS_Font_ProjectionMatrix;
            FPS_FontDrawing.DrawingPimitiveses.Clear();
            FPS_FontDrawing.Print(FPS_Font, FPS_RenderStr, new Vector3(7f, Settings.Window.Height - 7f, 0f), QFontAlignment.Left, FPS_Font_Options);
            FPS_FontDrawing.RefreshBuffers();
            FPS_FontDrawing.Draw();
            GL.Disable(EnableCap.Blend);
        }
        #endregion

        public static void CalcFPS(double Time)
        {
            Elapsed[ElapsedIndex] = Time;
            ElapsedIndex++;

            if (ElapsedIndex >= ElapsedMax)
            {
                double elapsed = 0.0;
                ElapsedIndex = 0;

                for (int i = 0; i < ElapsedMax; i++)
                    elapsed += Elapsed[i];

                RealFPS = ElapsedMax / elapsed;
            }
            Font_Render();
        }
    }
}
