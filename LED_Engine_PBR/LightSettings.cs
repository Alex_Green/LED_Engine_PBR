﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using OpenTK;

namespace LED_Engine_PBR
{
    public partial class LightSettings : Form
    {
        int LIndex = -1;
        bool ChangeFreeze = false;

        public LightSettings()
        {
            InitializeComponent();
            ListsRefresh();
        }

        private void LightSettings_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        void ListsRefresh()
        {
            listBox_Lights.Items.Clear();
            foreach (var l in Lights.LIGHTS)
                listBox_Lights.Items.Add(l.Name);

            comboBox_Type.Items.Clear();
            comboBox_Type.Items.AddRange(Enum.GetNames(typeof(LightType)));

            if (listBox_Lights.Items.Count != 0 && listBox_Lights.Items.Count > LIndex)
                listBox_Lights.SelectedIndex = LIndex;
        }

        private void checkedListBox_Lights_Click(object sender, EventArgs e)
        {
            LIndex = listBox_Lights.SelectedIndex;
            if (LIndex != -1)
            {
                ChangeFreeze = true;
                GetValues();
                ChangeFreeze = false;
            }
        }

        void GetValues()
        {
            checkBox_LEnabled.Checked = Lights.LIGHTS[LIndex].Enabled;
            
            comboBox_Type.Text = Lights.LIGHTS[LIndex].Type.ToString();
            Enable_Disable_TextBoxes();

            textBox_Name.Text = Lights.LIGHTS[LIndex].Name;

            sliderTrackbar_PosX.Value = (decimal)Lights.LIGHTS[LIndex].Position.X;
            sliderTrackbar_PosY.Value = (decimal)Lights.LIGHTS[LIndex].Position.Y;
            sliderTrackbar_PosZ.Value = (decimal)Lights.LIGHTS[LIndex].Position.Z;

            Vector2 DirectionYawPitch = Helper.ExtractYawPitch(Lights.LIGHTS[LIndex].Direction);
            sliderTrackbar_Yaw.Value = (decimal)((MathHelper.RadiansToDegrees(DirectionYawPitch.X) + 360f) % 360f);
            sliderTrackbar_Pitch.Value = (decimal)((MathHelper.RadiansToDegrees(DirectionYawPitch.Y) + 360f) % 360f);

            sliderTrackbar_R.Value = (decimal)Lights.LIGHTS[LIndex].Color.X;
            sliderTrackbar_G.Value = (decimal)Lights.LIGHTS[LIndex].Color.Y;
            sliderTrackbar_B.Value = (decimal)Lights.LIGHTS[LIndex].Color.Z;

            sliderTrackbar_Intensity.Value = (decimal)Lights.LIGHTS[LIndex].Intensity;

            sliderTrackbar_Attenuation.Value = (decimal)Lights.LIGHTS[LIndex].Attenuation;

            sliderTrackbar_SpotLight_CutOff.Value = (decimal)Lights.LIGHTS[LIndex].CutOFF;
            sliderTrackbar_SpotLight_Exp.Value = (decimal)Lights.LIGHTS[LIndex].Exponent;
        }

        void Enable_Disable_TextBoxes()
        {
            //Enable All
            sliderTrackbar_PosX.Enabled = true;
            sliderTrackbar_PosY.Enabled = true;
            sliderTrackbar_PosZ.Enabled = true;
            sliderTrackbar_Yaw.Enabled = true;
            sliderTrackbar_Pitch.Enabled = true;
            sliderTrackbar_Attenuation.Enabled = true;
            sliderTrackbar_SpotLight_CutOff.Enabled = true;
            sliderTrackbar_SpotLight_Exp.Enabled = true;

            switch (Lights.LIGHTS[LIndex].Type)
            {
                case LightType.Directional:
                    sliderTrackbar_PosX.Enabled = false;
                    sliderTrackbar_PosY.Enabled = false;
                    sliderTrackbar_PosZ.Enabled = false;
                    sliderTrackbar_Attenuation.Enabled = false;
                    sliderTrackbar_SpotLight_CutOff.Enabled = false;
                    sliderTrackbar_SpotLight_Exp.Enabled = false;
                    break;
                case LightType.Point:
                    sliderTrackbar_Yaw.Enabled = false;
                    sliderTrackbar_Pitch.Enabled = false;
                    sliderTrackbar_SpotLight_CutOff.Enabled = false;
                    sliderTrackbar_SpotLight_Exp.Enabled = false;
                    break;
            }
        }

        private void SomeChanged(object sender, EventArgs e)
        {
            if (LIndex != -1 && Lights.LIGHTS.Count > LIndex && !ChangeFreeze)
            {
                try
                {
                    Lights.LIGHTS[LIndex].Enabled = checkBox_LEnabled.Checked;
                    Lights.LIGHTS[LIndex].Type = (LightType)Enum.Parse(typeof(LightType), comboBox_Type.Text, true);
                    Enable_Disable_TextBoxes();
                    Lights.LIGHTS[LIndex].Name = textBox_Name.Text;

                    Lights.LIGHTS[LIndex].Position = new Vector3(
                        (float)sliderTrackbar_PosX.Value,
                        (float)sliderTrackbar_PosY.Value,
                        (float)sliderTrackbar_PosZ.Value);

                    Lights.LIGHTS[LIndex].Direction = Helper.FromYawPitch(
                        MathHelper.DegreesToRadians((float)sliderTrackbar_Yaw.Value),
                        MathHelper.DegreesToRadians((float)sliderTrackbar_Pitch.Value));

                    Lights.LIGHTS[LIndex].Color = new Vector3(
                        (float)sliderTrackbar_R.Value,
                        (float)sliderTrackbar_G.Value,
                        (float)sliderTrackbar_B.Value);

                    Lights.LIGHTS[LIndex].Intensity = (float)sliderTrackbar_Intensity.Value;

                    Lights.LIGHTS[LIndex].Attenuation = (float)sliderTrackbar_Attenuation.Value;

                    Lights.LIGHTS[LIndex].CutOFF = (float)sliderTrackbar_SpotLight_CutOff.Value;
                    Lights.LIGHTS[LIndex].Exponent = (float)sliderTrackbar_SpotLight_Exp.Value;

                    if (listBox_Lights.Items[LIndex].ToString() != textBox_Name.Text)
                        listBox_Lights.Items[LIndex] = textBox_Name.Text;
                }
                catch { }
            }
        }

        private void button_Add_Click(object sender, EventArgs e)
        {
            Light L = new Light();
            L.Name = "Light_" + DateTime.Now.ToLongTimeString();

            if (LIndex == -1)
                LIndex = Lights.LIGHTS.Count - 1;

            LIndex++;

            Lights.LIGHTS.Insert(LIndex, L);
            ListsRefresh();
        }

        private void button_Del_Click(object sender, EventArgs e)
        {
            if (LIndex != -1)
            {
                Lights.LIGHTS.RemoveAt(LIndex);
                LIndex--;
                if (LIndex == -1 && Lights.LIGHTS.Count > 0)
                    LIndex++;
                ListsRefresh();
            }
        }

        private void button_Serialize_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(SerializeToXml<List<Light>>(Lights.LIGHTS));
            MessageBox.Show("Lights was Serialized to Clipboard");
        }

        private void button_Deserialize_Click(object sender, EventArgs e)
        {
            try
            {
                Lights.LIGHTS = DeserializeFromXml<List<Light>>(Clipboard.GetText());
                ListsRefresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static string SerializeToXml<T>(T Obj)
        {
            StringBuilder SB = new StringBuilder();
            XmlWriter XmlWr = XmlWriter.Create(SB);
            XmlSerializer ser = new XmlSerializer(typeof(T));
            ser.Serialize(XmlWr, Obj);
            return SB.ToString();
        }

        public static T DeserializeFromXml<T>(string XmlStr)
        {
            T result;
            var ser = new XmlSerializer(typeof(T));
            using (var tr = new StringReader(XmlStr))
            {
                result = (T)ser.Deserialize(tr);
            }
            return result;
        }

        private void button_MoveLUp_Click(object sender, EventArgs e)
        {
            if (LIndex > 0)
            {
                Light L = Lights.LIGHTS[LIndex];
                Lights.LIGHTS[LIndex] = Lights.LIGHTS[LIndex - 1];
                Lights.LIGHTS[LIndex - 1] = L;
                LIndex--;
                ListsRefresh();
                listBox_Lights.SelectedIndex = LIndex;
            }
        }

        private void button_MoveLDown_Click(object sender, EventArgs e)
        {
            if (LIndex < listBox_Lights.Items.Count - 1)
            {
                Light L = Lights.LIGHTS[LIndex];
                Lights.LIGHTS[LIndex] = Lights.LIGHTS[LIndex + 1];
                Lights.LIGHTS[LIndex + 1] = L;
                LIndex++;
                ListsRefresh();
                listBox_Lights.SelectedIndex = LIndex;
            }
        }
    }
}
