﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using OpenTK.Platform;

namespace LED_Engine_PBR
{
    partial class Game : GameWindow
    {
        static string[] Args;
        static KeyboardState KeybrdState;

        [STAThread]
        private static void Main(string[] args)
        {
            Args = args;
            Application.EnableVisualStyles();
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture; //For fix parsing values like "0.5" and "0,5"

            using (GameWindow Window = new Game())
            {
                Window.Run(60.0, 200.0);
            }
        }
    }
}