﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace LED_Engine_PBR
{
    public static class Materials
    {
        public static List<Material> MATERIALS = new List<Material>(); // Loaded Materials
        public static List<Material> MaterialsList = new List<Material>(); // All Materials

        public static void LoadMaterialsList(string XmlFile, bool EngineContent)
        {
            try
            {
                XmlDocument XML = new XmlDocument();
                XmlNodeList xmlNodeList;

                XML.Load(XmlFile);
                xmlNodeList = XML.DocumentElement.SelectNodes("Material");

                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    Material material = new Material();

                    #region Parse
                    material.Name = xmlNode.SelectSingleNode("Name").InnerText;
                    material.EngineContent = EngineContent;

                    #region Shaders
                    string ShaderName = xmlNode.SelectSingleNode("Shader").InnerText;
                    for (int i = 0; i < Shaders.ShadersList.Count; i++)
                        if (Shaders.ShadersList[i].Name == ShaderName)
                        {
                            material.Shader = Shaders.ShadersList[i];
                            break;
                        }

                    if (material.Shader == null)
                        throw new Exception("LoadMaterialList() Parse Shaders Exception.");
                    #endregion

                    #region Textures
                    xmlNodeList = xmlNode.SelectNodes("Texture");
                    if (xmlNodeList.Count > 0)
                    {
                        for (int i = 0; i < xmlNodeList.Count; i++)
                        {
                            string TextureName = xmlNodeList.Item(i).SelectSingleNode("Name").InnerText;
                            uint TextureUnit = Convert.ToUInt32(xmlNodeList.Item(i).SelectSingleNode("TextureUnit").InnerText);

                            if (TextureUnit > material.Textures.Length | TextureName == null)
                                continue;

                            bool Found = false;
                            for (int j = 0; j < Textures.TexturesList.Count; j++)
                            {
                                if (Textures.TexturesList[j].Name == TextureName)
                                {
                                    material.Textures[TextureUnit] = Textures.TexturesList[j];
                                    Found = true;
                                    break;
                                }
                            }

                            if (!Found)
                                throw new Exception("LoadMaterialList() Parse Textures Exception. Name:\"" + TextureName + "\"");
                        }
                    }
                    #endregion

                    #region CullFace
                    xmlNodeList = xmlNode.SelectNodes("CullFace");
                    if (xmlNodeList.Count > 0)
                        material.CullFace = Convert.ToBoolean(xmlNodeList.Item(0).InnerText);
                    #endregion

                    #region Transparent
                    xmlNodeList = xmlNode.SelectNodes("Transparent");
                    if (xmlNodeList.Count > 0)
                        material.Transparent = Convert.ToBoolean(xmlNodeList.Item(0).InnerText);
                    #endregion

                    #region BaseColor (Albedo)
                    xmlNodeList = xmlNode.SelectNodes("BaseColor");
                    if (xmlNodeList.Count > 0)
                    {
                        try
                        {
                            string[] Color = xmlNodeList.Item(0).InnerText.Split(
                                new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                            float R, G, B, A;
                            switch (Color.Length)
                            {
                                case 1:
                                    R = float.Parse(Color[0]);
                                    material.BaseColor = new Vector4(R, R, R, 1.0f);
                                    break;
                                case 3:
                                    R = float.Parse(Color[0]);
                                    G = float.Parse(Color[1]);
                                    B = float.Parse(Color[2]);
                                    material.BaseColor = new Vector4(R, G, B, 1.0f);
                                    break;
                                case 4:
                                    R = float.Parse(Color[0]);
                                    G = float.Parse(Color[1]);
                                    B = float.Parse(Color[2]);
                                    A = float.Parse(Color[3]);
                                    material.BaseColor = new Vector4(R, G, B, A);
                                    break;
                                default:
                                    break;
                            }
                        }
                        catch { }
                    }
                    else
                    {
                        int Count = 0;
                        for (int i = 0; i < material.Textures.Length; i++)
                            if (material.Textures[i] != null)
                                Count++;
                        if (Count > 0)
                            material.BaseColor = new Vector4(1.0f);
                    }
                    #endregion

                    #region Roughness
                    xmlNodeList = xmlNode.SelectNodes("Roughness");
                    if (xmlNodeList.Count > 0)
                        material.Roughness = float.Parse(xmlNodeList.Item(0).InnerText);
                    #endregion

                    #region Metalness
                    xmlNodeList = xmlNode.SelectNodes("Metalness");
                    if (xmlNodeList.Count > 0)
                        material.Metalness = float.Parse(xmlNodeList.Item(0).InnerText);
                    #endregion

                    #region Emissive
                    xmlNodeList = xmlNode.SelectNodes("Emissive");
                    if (xmlNodeList.Count > 0)
                        material.Emissive = float.Parse(xmlNodeList.Item(0).InnerText);
                    #endregion

                    #region Ambient Occlusion
                    xmlNodeList = xmlNode.SelectNodes("AmbientOcclusion");
                    if (xmlNodeList.Count > 0)
                        material.AmbientOcclusion = float.Parse(xmlNodeList.Item(0).InnerText);
                    #endregion

                    #region Parallax Mapping
                    xmlNodeList = xmlNode.SelectNodes("ParallaxMapping");
                    if (xmlNodeList.Count > 0)
                    {
                        var TempNodeList = xmlNodeList[0].SelectNodes("Enabled");
                        if (TempNodeList.Count > 0)
                            material.UseParallaxMapping = bool.Parse(TempNodeList.Item(0).InnerText);

                        TempNodeList = xmlNodeList[0].SelectNodes("Scale");
                        if (TempNodeList.Count > 0)
                            material.Parallax_Scale = float.Parse(TempNodeList.Item(0).InnerText);

                        TempNodeList = xmlNodeList[0].SelectNodes("MinSteps");
                        if (TempNodeList.Count > 0)
                            material.Parallax_MinSteps = float.Parse(TempNodeList.Item(0).InnerText);

                        TempNodeList = xmlNodeList[0].SelectNodes("MaxSteps");
                        if (TempNodeList.Count > 0)
                            material.Parallax_MaxSteps = float.Parse(TempNodeList.Item(0).InnerText);

                        TempNodeList = xmlNodeList[0].SelectNodes("DiscardFragments");
                        if (TempNodeList.Count > 0)
                            material.Parallax_DiscardFragments = bool.Parse(TempNodeList.Item(0).InnerText);
                    }
                    #endregion
                    #endregion

                    MaterialsList.Add(material);
                }
            }
            catch (Exception e)
            {
                Log.WriteLineRed("Materials.LoadMaterialsList() Exception.");
                Log.WriteLineRed("XmlFile: \"{0}\", EngineContent: \"{1}\"", XmlFile, EngineContent);
                Log.WriteLineYellow(e.Message);
            }
        }

        public static Material GetMaterial(string Name)
        {
            foreach (var i in MATERIALS)
                if (i.Name.GetHashCode() == Name.GetHashCode())
                    return i;
            return null;
        }

        public static Material Load(string Name)
        {
            try
            {
                Material M = GetMaterial(Name);

                if (M == null)
                {
                    foreach (var v in MaterialsList)
                        if (v.Name.GetHashCode() == Name.GetHashCode())
                            M = v;
                    if (M == null)
                        return null;
                }

                if (M.UseCounter == 0)
                {
                    Shaders.Load(M.Shader.Name);

                    for (int i = 0; i < M.Textures.Length; i++)
                        if (M.Textures[i] != null)
                            Textures.Load(M.Textures[i].Name);

                    MATERIALS.Add(M);
                }

                M.UseCounter++;
                return M;
            }
            catch
            {
                Log.WriteLineRed("Materials.Load() Exception, Name: \"{0}\"", Name);
                return null;
            }
        }

        public static void Unload(string Name)
        {
            if (Name == null)
                return;

            Unload(GetMaterial(Name));
        }

        public static void Unload(Material M)
        {
            if (M != null)
            {
                if (M.UseCounter > 0)
                {
                    M.UseCounter--;

                    if (M.UseCounter == 0)
                    {
                        M.Free();
                        MATERIALS.Remove(M);
                    }
                }
            }
        }
    }

    public class Material
    {
        public uint UseCounter = 0;
        public bool EngineContent = false;

        public string Name = String.Empty;
        public Shader Shader;
        public bool CullFace = true;
        public bool Transparent = false;

        static Random R = new Random();
        public Vector4 BaseColor = new Vector4((float)R.NextDouble(), (float)R.NextDouble(), (float)R.NextDouble(), 1.0f);
        public float Roughness = 0.5f;
        public float Metalness = 0.0f;
        public float Emissive = 1.0f; // EmissiveScale
        public float AmbientOcclusion = 1.0f;

        // Parallax Mapping
        public bool UseParallaxMapping = false;
        public float Parallax_Scale = 0.08f;
        public float Parallax_MinSteps = 32.0f;
        public float Parallax_MaxSteps = 64.0f;
        public bool Parallax_DiscardFragments = false;

        public Texture[] Textures = new Texture[Settings.GL.TextureImageUnits];

        public Material()
        {
        }

        public Material(Material Original)
        {
            Shader = Shaders.Load(Original.Shader.Name);

            CullFace = Original.CullFace;
            Transparent = Original.Transparent;
            BaseColor = Original.BaseColor;
            Metalness = Original.Metalness;
            Roughness = Original.Roughness;
            Emissive = Original.Emissive;
            AmbientOcclusion = Original.AmbientOcclusion;

            // Parallax Mapping
            UseParallaxMapping = Original.UseParallaxMapping;
            Parallax_Scale = Original.Parallax_Scale;
            Parallax_MinSteps = Original.Parallax_MinSteps;
            Parallax_MaxSteps = Original.Parallax_MaxSteps;
            Parallax_DiscardFragments = Original.Parallax_DiscardFragments;

            Textures = new Texture[Original.Textures.Length];
            for (int i = 0; i < Textures.Length; i++)
                if (Original.Textures[i] != null)
                    Textures[i] = LED_Engine_PBR.Textures.Load(Original.Textures[i].Name);
        }

        public void Free()
        {
            Shaders.Unload(Shader);

            for (int i = 0; i < Textures.Length; i++)
                LED_Engine_PBR.Textures.Unload(Textures[i]);

            UseCounter = 0;
        }
    }
}
