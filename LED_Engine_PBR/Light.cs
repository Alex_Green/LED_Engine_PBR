﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace LED_Engine_PBR
{
    public static class Lights
    {
        public static List<Light> LIGHTS = new List<Light>();
    }

    public enum LightType
    {
        Directional,
        Point,
        Spot,
    }

    public class Light
    {
        LightType type = LightType.Point;

        public bool Enabled = true;
        public string Name = String.Empty;
        public Vector3 Color = new Vector3(1.0f);
        public float Intensity = 1.0f;
        public Vector3 Position = new Vector3(0.0f); //Position in eye coords
        Vector3 direction = new Vector3(0.0f, MathHelper.PiOver2, 0.0f);
        public float Attenuation = 2.2f; //Attenuation exponent
        float cutOff = 45.0f;
        public float Exponent = 2.0f;

        public Light()
        {
        }

        public Light(LightType LType)
        {
            Type = LType;
        }

        public LightType Type
        {
            get { return type; }
            set
            {
                type = value;

                if (type == LightType.Spot)
                    ClampSpotLightAngle();
            }
        }

        public Vector3 Direction
        {
            get { return direction; }
            set
            {
                direction = value;
                direction.Normalize();
            }
        }

        void ClampSpotLightAngle()
        {
            if (cutOff > 90.0f)
                cutOff = 90.0f;
            if (cutOff < 0.0f)
                cutOff = 0.0f;
        }

        public float CutOFF
        {
            get { return cutOff; }
            set
            {
                cutOff = value;
                ClampSpotLightAngle();
            }
        }
    }
}
